import React, { useState, Fragment }  from 'react'

const TransactionElement = props => {
	const [state, setState] = useState({
		extraDataVisible: false,
	})

	const toggleExtraData = () => {
		setState({
			extraDataVisible: !state.extraDataVisible
		})
	}

	return (
		<li className={`transactions-list-element`} key={props.transaction.id}>
			<div className={`transactions-list-element-column transactions-list-element-${props.transaction.type}`}>
				{props.transaction.type}
			</div>
			<div className='transactions-list-element-column'>
				${props.transaction.amount}
			</div>
			<div className='transactions-list-element-column extraData'>
				{ 
					state.extraDataVisible ? (
						<Fragment>
							<div className='transactions-list-element-column'>
								Id: {props.transaction.id}
							</div>
							<div className='transactions-list-element-column'>
								Date: {props.transaction.effectiveDate}
							</div>
						</Fragment>
					) : (
						<button onClick={toggleExtraData}>View details</button>
					)
				}
			</div>
		</li>
	)
}

export default TransactionElement