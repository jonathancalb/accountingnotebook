import React, { useState, useEffect } from 'react'
// Styles should be modularized with each component but for this example 
// we just style all in this App.css file.
import './App.css'
import TransactionElement from './components/TransactionElement/TransactionElement'

function App() {
	const [state, setState] = useState({
		transactions: [],
	})
	
	useEffect(() => {
		fetch('http://localhost:4000/transactions')
			.then(response => response.json())
			.then(result => {
				setState({
					transactions: result,
				})
			})
			.catch(error => console.log('error', error));
	}, [])

	return (
		<div className="app">
			<ul className='transactions-list'>
				<li key='header' className='transactions-list-element transactions-list-element-header'>
					<div className='transactions-list-element-column'>Type</div>
					<div className='transactions-list-element-column'>Amount</div>
					<div className='transactions-list-element-column'>Extra data</div>
				</li>
			{
				state.transactions.map(transaction => (
					<TransactionElement transaction={transaction} />
				))
			}
			</ul>
		</div>
	);
}

export default App;
