# AccountingNotebook

Ideally client and server would be in different repositories but for ease of use we did them together in the same repository for this example.

# Client

* Go to the `/client` folder.
* Install dependencies with `npm i`.
* Start server doing `npm start`.
* Client will be running on `localhost:3000`

<br />

* The build files are located in the `/client/build` folder.
* To see the build working you will need to spin up a webserver.
Otherwise just do `npm start` as mentioned above at it will work.

<br />

* The client project was done with `create-react-app` to have the ground for the project very fast.
* The README.md inside client is regarding that but should be updated eventually for the project needs.

# Server

* Go to the `/server` folder.
* Install dependencies with `npm i`.
* Start server doing `npm start`.
* Server will be running on `localhost:4000`