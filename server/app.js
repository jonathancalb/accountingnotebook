const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const app = express()
const port = 4000

app.use(bodyParser.json())
app.use(cors())

// In a real scenario users and transactions would be stored in the db
// but for this example we store all data in memory here:
let users = {
	'1234': {
		debit: {
			balance: 2000
		},
		credit: {
			balance: 9900
		},
	}
}
let transactions = [
	{
		id: '0',
		type: 'credit',
		amount: 100,
		effectiveDate: 1599603760990
	}
]
 
// Helper functions

function transformDate(dateTime){
	const date = new Date(dateTime)
	return date.toString('en-US')
}

function formatTransactionForClient(transaction) {
	return {
		...transaction,
		effectiveDate: transformDate(transaction.effectiveDate)
	}
}

function userCanDoTransaction(amount, type) {
	return users['1234'][type].balance - amount >= 0
}

function saveTransaction(transaction) {
	users['1234'][transaction.type].balance -= transaction.amount

	transactions.push(transaction)
}

// Endpoints

app.get('/accounts/:id', (req, res) => {
	if (users[req.params.id]) {
		res.json(users[req.params.id])
	} else {
		res.status(404).send()
	}
})

app.get('/transactions', (req, res) => {
	const formattedTransactions = transactions.map(formatTransactionForClient)
	res.json(formattedTransactions)
})

app.get('/transactions/:id', (req, res) => {
	const transaction = transactions.find(transaction => transaction.id === req.params.id)
	if (transaction) {
		res.json(formatTransactionForClient(transaction))
	} else {
		res.status(404).send()
	}
})

app.post('/transactions', (req, res) => {
	const { type, amount } = req.body;
	if (
		['credit', 'debit'].includes(type)
		&& amount > 0
	) { 
		if (userCanDoTransaction(amount, type)) {
			const transaction = {
				id: transactions.length.toString(),
				type,
				amount,
				effectiveDate: Date.now()
			}
			saveTransaction(transaction)
			res.send(formatTransactionForClient(transaction))
		} else {
			res.status(400).send({
				message: 'User can\'t do this transaction'
			})
		}
	} else {
		res.status(400).send({
			message: 'Wrong type or value'
		})
	}
})

app.listen(port, () => {
  console.log(`Accounting Notebook app listening at http://localhost:${port}`)
})